using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deeper.model;

namespace deeper
{
    [CreateAssetMenu(fileName = "TimeBasedSpawnRateBlockSpec", menuName = "deeper/TimeBasedSpawnRateBlockSpec")]
    public class TimeBasedSpawnRateBlockSpec : AbstractBlockSpec
    {
        [SerializeField] private string _name;
        public override string Name => _name;

        [SerializeField] private float _minValue;
        [SerializeField] private float _maxValue;

        [SerializeField] private float _spawnRateTimeMultiplier;
        [SerializeField] private int _baseSpawnRate;
        public override int SpawnRate => (int)(_spawnRateTimeMultiplier * Time.time + _baseSpawnRate);
        public override float GenValue()
        {
            return Random.value * (_maxValue - _minValue) + _minValue;
        }

        [SerializeField] private float _hp;
        public override float GenHp() { return _hp; }


        public TimeBasedSpawnRateBlockSpec(string name, int minValue, int maxValue, float hp)
        {
            _name = name;
            _minValue = minValue;
            _maxValue = maxValue;
            _hp = hp;
        }
    }
}
