using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName="Upgrade", menuName = "deeper/Upgrade")]
public class Upgrade : ScriptableObject
{
    public string upgradeName;
    public int[] costs = new int[6];
    public float[] levelValues = new float[6];
    public int costIncrement;
    public float valueIncrement; 
    public bool multiplier = false;  
}
