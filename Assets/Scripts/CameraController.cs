using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineFreeLook))]
public class CameraController : MonoBehaviour
{
    private CinemachineFreeLook freeLookCam;
    public int mouseButton = 0;

    private void Start() {
        freeLookCam = GetComponent<CinemachineFreeLook>();
        freeLookCam.m_XAxis.m_InputAxisName = "";
        freeLookCam.m_YAxis.m_InputAxisName = "";
    }

    private void Update() {
        // only pivot around player while button is being held down
        if(Input.GetMouseButton(mouseButton)){
            freeLookCam.m_XAxis.m_InputAxisName = "Mouse X";
            freeLookCam.m_YAxis.m_InputAxisName = "Mouse Y";
        }else{
            freeLookCam.m_XAxis.m_InputAxisName = "";
            freeLookCam.m_YAxis.m_InputAxisName = "";
        }
    }
}
