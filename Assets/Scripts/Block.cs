
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Random = UnityEngine.Random;

namespace deeper.model
{ 
    public interface IBlock
    {
        public float Value { get; }
        public float Hp { get; }
    }

    public interface IBlockSpec
    {
        public int SpawnRate { get; }
        public string Name { get; }
        public float GenValue();
        public float GenHp();
    }

    public abstract class AbstractBlockSpec : ScriptableObject, IBlockSpec
    {
        public abstract string Name { get; }
        public abstract int SpawnRate { get; }

        public abstract float GenHp();

        public abstract float GenValue();
    }


    [Serializable]
    public class Block : IBlock
    {
        private IBlockSpec _spec;
        public IBlockSpec BlockSpec => _spec;

        private float _value;
        public float Value => _value;

        private float _startHp;
        public float StartHp => _startHp;

        private float _hp;
        public float Hp => _hp;

        public Block(IBlockSpec spec)
        {
            _spec = spec;
            _value = _spec.GenValue();
            _hp = _spec.GenHp();
            _startHp = _hp;
        }

        public void Damage(float damage)
        {
            _hp -= damage;
        }
    }
}
