using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace deeper
{
    public class CameraBasedPos : MonoBehaviour
    {
        [SerializeField] private Transform _center;
        [SerializeField] private float _radius = 2f;

        void LateUpdate()
        {
            Position();
        }

        private void Position()
        {
            transform.position = _center.position + Camera.main.transform.right * _radius;
        }
    }

}
