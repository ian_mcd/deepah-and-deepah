using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deeper.model;
using System;

namespace deeper
{
    [CreateAssetMenu(menuName = "deeper/BlockPrefabs", fileName = "BlockPrefabs")]
    public class BlockPrefabs : ScriptableObject
    {
        public AbstractBlockSpec[] Specs;
        public BlockObject[] Prefabs;

        public BlockObject GetPrefab(AbstractBlockSpec blockSpec)
        {
            int idx = Array.IndexOf(Specs, blockSpec);
            return Prefabs[idx];
        }

        public bool Contains(AbstractBlockSpec blockSpec)
        {
            return Array.Exists(Specs, b => b == blockSpec);
        }
    }
}