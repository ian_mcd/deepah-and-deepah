using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace deeper.UI
{
    [RequireComponent(typeof(Image))]
    public class Healthbar : MonoBehaviour
    {
        public TextMeshProUGUI healthNum;
        private RectTransform _rt;
        private float _fullX;
        [SerializeField] private BlockObject _bo;
        [SerializeField] private float _displayTime;

        private void Start()
        {
            _rt = GetComponent<RectTransform>();
            _fullX = _rt.rect.width * _rt.localScale.x;
            _bo.OnDamage += UpdateBar;
            if (healthNum != null)
                healthNum.text = ((int)_bo.Block.Hp).ToString();
        }

        private void UpdateBar()
        {
            if (healthNum != null)
                healthNum.text = ((int) _bo.Block.Hp).ToString();
            float fullness = 1.0f - (float)_bo.Block.Hp / _bo.Block.StartHp;
            _rt.sizeDelta = new Vector2(-fullness * _fullX, _rt.sizeDelta.y);
        }
    }
}
