using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace deeper
{
    public class CoinGainEffect : MonoBehaviour
    {
        [SerializeField] private TMP_Text _text;
        private string _format;
        [SerializeField] private AnimationCurve _scaleCurve;
        [SerializeField] private float _length;
        [SerializeField] private float _distance = 1;
        private Vector3 _startScale;
        void Awake()
        {
            _format = _text.text;
        }

        public void SetValue(int value)
        {
            _text.text = string.Format(_format, value);
            _startScale = transform.localScale;
        }

        private void Start()
        {
            StartCoroutine(ScaleRoutine());
        }

        private IEnumerator ScaleRoutine()
        {
            float t = 0;
            while (t < _length)
            {
                float v = t / _length;
                transform.localScale = _startScale * _scaleCurve.Evaluate(v);
                transform.position += v * _distance * Vector3.up;
                yield return null;
                t += Time.deltaTime;
            }
            Destroy(gameObject);
        }
    }
}
