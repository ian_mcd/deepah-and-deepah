using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverTime : MonoBehaviour
{
    [SerializeField] private float _destructionTime;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(_destructionTime);
        Destroy(gameObject);
    }
}
