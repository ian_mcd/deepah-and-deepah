using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using deeper.model;

namespace deeper.UI
{
    public class UIController : MonoBehaviour
    {
        // ella UI stats
        public TextMeshProUGUI ellaSpeedUI;
        private string ellaSpeedFormat;
        public TextMeshProUGUI ellaPowerUI;
        private string ellaPowerFormat;

        // player UI stats
        public TextMeshProUGUI yourPowerUI;
        private string yourPowerFormat;
        public TextMeshProUGUI yourLuckUI;
        private string yourLuckFormat;
        public TextMeshProUGUI blocksBrokenUI;
        private string blocksBrokenFormat;
        private int blocksBrokenTotal;
        public TextMeshProUGUI playTimeUI;
        private string playTimeFormat;
        public TextMeshProUGUI totalCoinCountUI;
        private string totalCoinCountFormat;
        private int totalCoinCountVal;
        public TextMeshProUGUI currentCoinCountUI;
        private string currentCointFormat;
        // same order as BlockTypes in BlockManager
        public List<TextMeshProUGUI> blockCountsUI = new List<TextMeshProUGUI>();

        // upgrades
        public TextMeshProUGUI ellaSpeedUpgradeUI;
        public TextMeshProUGUI ellaSpeedIncrementUI;
        public TextMeshProUGUI ellaSpeedCostUI;

        public TextMeshProUGUI ellaPowUpgradeUI;
        public TextMeshProUGUI ellaPowIncrementUI;
        public TextMeshProUGUI ellaPowCost;

        public TextMeshProUGUI yourPowerUpgradeUI;
        public TextMeshProUGUI yourPowerIncrementUI;
        public TextMeshProUGUI yourPowerCostUI;

        public TextMeshProUGUI luckUpgradeUI;
        public TextMeshProUGUI luckIncrementUI;
        public TextMeshProUGUI luckCostUI;


        // references
        BlockManager blockManager; 
        PlayerManager playerManager;
        UpgradeController upgradeController;
        
        // vars 
        private Dictionary<IBlockSpec, int> blockCounts = 
            new Dictionary<IBlockSpec, int>();
        private Dictionary<IBlockSpec, (TextMeshProUGUI GUI, string format)> blockTypeUIInfo = 
            new Dictionary<IBlockSpec, (TextMeshProUGUI GUI, string format)>();
        private Dictionary<string, UpgradeUI> upgradeUIs = new Dictionary<string, UpgradeUI>();

        void Start()
        {
            // add listeners
            blockManager = deeper.BlockManager.Instance;
            blockManager.OnBlockDestroy += updateBlockCounts;
            playerManager = deeper.PlayerManager.instance;
            playerManager.CoinListener += updateCoins;
            playerManager.StatListeners[PlayerManager.LUCK] += upgradeYourLuck;
            playerManager.StatListeners[PlayerManager.ELLA_DAMAGE] += updateEllaPower;
            playerManager.StatListeners[PlayerManager.ELLA_SPEED] += updateEllaSpeed;
            playerManager.StatListeners[PlayerManager.PLAYER_DAMAGE] += updateYourPower;
            upgradeController = UpgradeController.Instance;
            upgradeController.upgradeObjs[upgradeController.EllaDamageName].evnt.AddListener(updateEllaPowUpgrade);
            upgradeController.upgradeObjs[upgradeController.EllaSpeedName].evnt.AddListener(updateEllaSpeedUpgrade);
            upgradeController.upgradeObjs[upgradeController.YourDamageName].evnt.AddListener(updateYourPowUpgrade);
            upgradeController.upgradeObjs[upgradeController.LuckName].evnt.AddListener(updateLuckUpgrade);

            // initialize block count related items
            for(int i = 0; i < blockCountsUI.Count; i++){
                // keep track of block counts
                TextMeshProUGUI countUI = blockCountsUI[i];
                blockCounts.Add(blockManager.BlockTypes[i], 0);
                // keep track of UI info
                blockTypeUIInfo.Add(blockManager.BlockTypes[i], 
                    (countUI, countUI.text));

                // initialize all UI
                countUI.text = string.Format(countUI.text, 0);
            }
            
            // initialize formats
            ellaSpeedFormat = ellaSpeedUI.text;
            ellaPowerFormat = ellaPowerUI.text;
            yourPowerFormat = yourPowerUI.text;
            yourLuckFormat = yourLuckUI.text;
            blocksBrokenFormat = blocksBrokenUI.text;
            playTimeFormat = playTimeUI.text;
            totalCoinCountFormat = totalCoinCountUI.text;
            currentCointFormat = currentCoinCountUI.text;

            // initialize values
            totalCoinCountVal = playerManager.coins;
            ellaSpeedUI.text = string.Format(ellaSpeedFormat, playerManager[PlayerManager.ELLA_DAMAGE]);
            ellaPowerUI.text = string.Format(ellaPowerFormat, playerManager[PlayerManager.ELLA_SPEED]);
            yourPowerUI.text = string.Format(yourPowerFormat, playerManager[PlayerManager.PLAYER_DAMAGE]);
            yourLuckUI.text = string.Format(yourLuckFormat, 1);
            blocksBrokenUI.text = string.Format(blocksBrokenFormat, 0.0f);
            playTimeUI.text = string.Format(playTimeFormat,0);
            totalCoinCountUI.text = string.Format(totalCoinCountFormat, totalCoinCountVal);
            currentCoinCountUI.text = string.Format(currentCointFormat, totalCoinCountVal);

            // initialize updates
            upgradeUIs.Add(upgradeController.EllaSpeedName, new UpgradeUI(ellaSpeedUpgradeUI, ellaSpeedIncrementUI, ellaSpeedCostUI));
            upgradeUIs.Add(upgradeController.EllaDamageName, new UpgradeUI(ellaPowUpgradeUI, ellaPowIncrementUI, ellaPowCost));
            upgradeUIs.Add(upgradeController.YourDamageName, new UpgradeUI(yourPowerUpgradeUI, yourPowerIncrementUI, yourPowerCostUI));
            upgradeUIs.Add(upgradeController.LuckName, new UpgradeUI(luckUpgradeUI, luckIncrementUI, luckCostUI));
        }

        // Update is called once per frame
        void Update()
        {
            playTimeUI.text = string.Format(playTimeFormat,(int)Time.time);
        }

        public void updateEllaSpeedUpgrade(UpgradeObject upgrade){
            UpgradeUI UI = upgradeUIs[upgradeController.EllaSpeedName];
            UI.setCost(upgrade.nextCost);
            UI.setIncrement(upgrade.increment);
            UI.setLevel(upgrade.Level);
        }

        public void updateEllaPowUpgrade(UpgradeObject upgrade){
            UpgradeUI UI = upgradeUIs[upgradeController.EllaDamageName];
            UI.setCost(upgrade.nextCost);
            UI.setIncrement(upgrade.increment);
            UI.setLevel(upgrade.Level);
        }

        public void updateYourPowUpgrade(UpgradeObject upgrade){
            UpgradeUI UI = upgradeUIs[upgradeController.YourDamageName];
            UI.setCost(upgrade.nextCost);
            UI.setIncrement(upgrade.increment);
            UI.setLevel(upgrade.Level);
        }

        public void updateLuckUpgrade(UpgradeObject upgrade){
            UpgradeUI UI = upgradeUIs[upgradeController.LuckName];
            UI.setCost(upgrade.nextCost);
            UI.setIncrement(upgrade.increment);
            UI.setLevel(upgrade.Level);
        }

        /// <summary>
        /// Update all the block counts in the UI
        /// </summary>
        /// <param name="block"></param>
        public void updateBlockCounts(Block block){
            // update individual coin counts
            int count = ++blockCounts[block.BlockSpec];
            var UITuple = blockTypeUIInfo[block.BlockSpec];
            UITuple.GUI.text = string.Format(UITuple.format, count);  
            
            // update total blocks broken
            blocksBrokenTotal++;
            blocksBrokenUI.text = string.Format(blocksBrokenFormat, blocksBrokenTotal);

            // update total coins
            totalCoinCountVal += (int)block.Value;
            totalCoinCountUI.text = string.Format(totalCoinCountFormat, totalCoinCountVal);
        }

        /// <summary>
        /// Update current coin count UI
        /// </summary>
        /// <param name="coins">the current coin count</param>
        public void updateCoins(int coins){
            currentCoinCountUI.text = string.Format(currentCointFormat, coins);            
        }

        /// <summary>
        /// Update Ella power UI
        /// </summary>
        /// <param name="val">the value of Ella's power</param>
        public void updateEllaPower(float val){
            ellaPowerUI.text = string.Format(ellaPowerFormat, val);
        }

        /// <summary>
        /// Upgrade Ella speed UI
        /// </summary>
        /// <param name="val">the value of Ella's speed</param>
        public void updateEllaSpeed(float val){
            ellaSpeedUI.text = string.Format(ellaSpeedFormat, val);
        }

        /// <summary>
        /// Ugrade your power UI
        /// </summary>
        /// <param name="val">the value of your current power</param>
        public void updateYourPower(float val){
            yourPowerUI.text = string.Format(yourPowerFormat, val);
        }

        /// <summary>
        /// Upgrade luck UI
        /// </summary>
        /// <param name="val">the current value of luck</param>
        public void upgradeYourLuck(float val){
            yourLuckUI.text = string.Format(yourLuckFormat, val);
        }

        private void OnDestroy() {
            blockManager.OnBlockDestroy -= updateBlockCounts;
            playerManager.CoinListener -= updateCoins;
            playerManager.StatListeners[PlayerManager.LUCK] -= upgradeYourLuck;
            playerManager.StatListeners[PlayerManager.ELLA_DAMAGE] -= updateEllaPower;
            playerManager.StatListeners[PlayerManager.ELLA_SPEED] -= updateEllaSpeed;
            playerManager.StatListeners[PlayerManager.PLAYER_DAMAGE] -= updateYourPower;
        } 
    }

    public class UpgradeUI
    {
        public TextMeshProUGUI LevelUI;
        public TextMeshProUGUI IncrementUI;
        public TextMeshProUGUI CostUI;
        public string LevelFormat;
        public string IncrementFormat;
        public string CostFormat;
        
        public UpgradeUI(TextMeshProUGUI LevelUI, TextMeshProUGUI IncrementUI, TextMeshProUGUI CostUI){
            this.LevelUI = LevelUI;
            this.IncrementUI = IncrementUI;
            this.CostUI = CostUI;
            LevelFormat = LevelUI.text;
            CostFormat = CostUI.text;
            IncrementFormat = IncrementUI.text;
            initialize();
        }

        private void initialize(){
            LevelUI.text = string.Format(LevelFormat, 0);
            CostUI.text = string.Format(CostFormat, 0);
            IncrementUI.text = string.Format(IncrementFormat, 0);
        }

        public void setLevel(int val){
            LevelUI.text = string.Format(LevelFormat, val);
        }

        public void setIncrement(float val){
            IncrementUI.text = string.Format(IncrementFormat, val);
        }

        public void setCost(int val){
            CostUI.text = string.Format(CostFormat, val);
        }
    }
}
