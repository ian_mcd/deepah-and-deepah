using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using deeper;
using UnityEngine.Events;

namespace deeper.UI
{
    public class UpgradeController : MonoBehaviour
    {
        private static UpgradeController _instance;
        public static UpgradeController Instance => (_instance) ? _instance : _instance = FindObjectOfType<UpgradeController>();
        public string EllaSpeedName = "Ella Speed";
        public string EllaDamageName = "Ella Damage";
        public string YourDamageName = "Player Damage";
        public string LuckName = "Luck";
        public List<Upgrade> Upgrades = new List<Upgrade>();
        public Dictionary<string, (UpgradeObject obj, UnityEvent<UpgradeObject> evnt)> upgradeObjs =
            new Dictionary<string, (UpgradeObject obj, UnityEvent<UpgradeObject> evnt)>();
        private PlayerManager playerManager;
        
        private void Start() {
            foreach(Upgrade upgrade in Upgrades){
                upgradeObjs.Add(upgrade.name, (new UpgradeObject(upgrade), new UnityEvent<UpgradeObject>()));
            }   

            playerManager = PlayerManager.instance;

            StartCoroutine(initializeUI());
        }

        private IEnumerator initializeUI(){
            yield return new WaitForEndOfFrame();
            foreach(string name in upgradeObjs.Keys){
                var tuple = upgradeObjs[name];
                tuple.evnt.Invoke(tuple.obj);
            }
        }

        public void UpgradeEllaSpeed(){
            var upgrade = upgradeObjs[EllaSpeedName];
            if(upgrade.obj.nextCost <= playerManager.coins){
                playerManager.SubCoins(upgrade.obj.nextCost);
                upgrade.obj.levelUp();
                playerManager.UpgradePercentStat(PlayerManager.ELLA_SPEED, upgrade.obj.increment);
                upgrade.evnt.Invoke(upgrade.obj);      
            }
            
        }

        public void UpgradeEllaDamage(){
            var upgrade = upgradeObjs[EllaDamageName];
            if(upgrade.obj.nextCost <= playerManager.coins){
                playerManager.SubCoins(upgrade.obj.nextCost);
                upgrade.obj.levelUp();
                playerManager.UpgradePercentStat(PlayerManager.ELLA_DAMAGE, upgrade.obj.increment);
                upgrade.evnt.Invoke(upgrade.obj);
            }
        }

        public void UpgradeYourDamage(){
            var upgrade = upgradeObjs[YourDamageName];
            if(upgrade.obj.nextCost <= playerManager.coins){
                playerManager.SubCoins(upgrade.obj.nextCost);
                upgrade.obj.levelUp();
                playerManager.UpgradePercentStat(PlayerManager.PLAYER_DAMAGE, upgrade.obj.increment);
                upgrade.evnt.Invoke(upgrade.obj);
            }
        }

        public void upgradeLuck(){
            var upgrade = upgradeObjs[LuckName];
            if(upgrade.obj.nextCost <= playerManager.coins){
                playerManager.SubCoins(upgrade.obj.nextCost);
                upgrade.obj.levelUp();
                playerManager.UpgradePercentStat(PlayerManager.LUCK, upgrade.obj.increment);
                upgrade.evnt.Invoke(upgrade.obj);
            }
        }
        
    } 

    public class UpgradeObject{
        public int Level;
        public float Value;  
        public int Cost;
        public Upgrade Upgrade;
        public float increment;
        public int nextCost;

        public UpgradeObject(Upgrade upgrade){
            this.Upgrade = upgrade;
            Level = 0;
            Value = upgrade.levelValues[0];
            Cost = upgrade.costs[0];
            increment = upgrade.levelValues[1] - upgrade.levelValues[0];
            nextCost = upgrade.costs[1];
        }

        public void levelUp(){
            Level++;
            if(Level < Upgrade.levelValues.Length){
                Value = Upgrade.levelValues[Level];
                Cost = Upgrade.costs[Level];
            }else{
                Value = (Level - 4) * Upgrade.valueIncrement;
                Cost += Upgrade.costIncrement;
            }
            setIncrement();
            setNextCost();
        }

        private void setIncrement(){
            if(Level + 1 < Upgrade.levelValues.Length){
                increment = Upgrade.levelValues[Level + 1] - Upgrade.levelValues[Level];
            }else{  
                increment = Upgrade.valueIncrement;
            }
        }

        private void setNextCost(){
            if(Level + 1 < Upgrade.levelValues.Length){
                nextCost = Upgrade.costs[Level + 1];
            }else{  
                nextCost = Cost + Upgrade.costIncrement;
            } 
        }


    }                          
}
