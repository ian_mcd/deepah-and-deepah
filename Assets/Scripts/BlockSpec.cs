using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace deeper.model
{
    [CreateAssetMenu(fileName = "Block", menuName = "deeper/Block")]
    public class BlockSpec : AbstractBlockSpec
    {
        [SerializeField] private string _name;
        public override string Name => _name;

        [SerializeField] private int _value;
        public override float GenValue() { return _value; }

        [SerializeField] private int _spawnRate;
        public override int SpawnRate => _spawnRate;

        [SerializeField] private float _hp;
        public override float GenHp() { return _hp; }

        public BlockSpec(string name, int value, float hp)
        {
            _name = name;
            _value = value;
            _hp = hp;
        }
    }
}