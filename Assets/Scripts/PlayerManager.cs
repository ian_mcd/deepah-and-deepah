using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deeper.model;
using System;

namespace deeper
{
    public class PlayerManager : MonoBehaviour
    {
        // singleton
        private static PlayerManager _instance;
        public static PlayerManager instance => _instance ? _instance : _instance = FindObjectOfType<PlayerManager>();


        public static readonly StatKey PLAYER_DAMAGE = new StatKey("player damage");
        public static readonly StatKey ELLA_DAMAGE = new StatKey("ella damage");
        public static readonly StatKey ELLA_SPEED = new StatKey("ella speed");
        public static readonly StatKey LUCK = new StatKey("luck");
        // stats 
        public int coins;

        public event Action<int> CoinListener;

        public float this[StatKey k] => GetStatValue(k);

        public Dictionary<StatKey, float> Stats = new Dictionary<StatKey, float>()
        {
            { PLAYER_DAMAGE, 1 },
            { ELLA_DAMAGE, 1 },
            { ELLA_SPEED, 1 },
            { LUCK, 1 }
        };
        public Dictionary<StatKey, Action<float>> StatListeners = new Dictionary<StatKey, Action<float>>();

        private Dictionary<StatKey, float> _percentModifiers = new Dictionary<StatKey, float>();
        private Dictionary<StatKey, float> _incrementModifiers = new Dictionary<StatKey, float>();
        private Dictionary<AbstractBlockSpec, (StatKey ella, StatKey player)> _blockToKey = new Dictionary<AbstractBlockSpec, (StatKey ella, StatKey player)>();

        //public Dictionary<Item, int> inventory = new Dictionary<Item, int>();
        private Dictionary<model.AbstractBlockSpec, float> BlockTypeMultipliers = new Dictionary<model.AbstractBlockSpec, float>();
        [SerializeField] private CoinGainEffect _coinEffect;


        /// <summary>
        /// Get multiplier for the given block type
        /// </summary>
        /// <param name="blockType">A block spec of the desired block type</param>
        /// <returns>player multiplier</returns>
        public float getMultiplier(model.BlockSpec blockType){
            if(BlockTypeMultipliers.ContainsKey(blockType)){
                return BlockTypeMultipliers[blockType];
            } else {
                // default value for multiplier is 1
                BlockTypeMultipliers.Add(blockType, 1);
                return BlockTypeMultipliers[blockType];
            }
        }

        public float GetStatValue(StatKey key)
        {
            float value = Stats[key];
            value *= _percentModifiers[key];
            value += _incrementModifiers[key];
            return value;
        }

        public void UpgradePercentStat(StatKey key, float value)
        {
            _percentModifiers[key] += value;
            StatListeners[key]?.Invoke(this[key]);
        }

        public void UpgradeValueStat(StatKey key, float value)
        {
            _incrementModifiers[key] += value;
            StatListeners[key]?.Invoke(this[key]);
        }

        public void UpgradeBaseValueStat(StatKey key, float value)
        {
            Stats[key] += value;
            StatListeners[key]?.Invoke(this[key]);
        }

        private void Awake()
        {
            foreach (AbstractBlockSpec blocktype in BlockManager.Instance.BlockTypes)
            {
                var ellaKey = new StatKey($"ella {blocktype.Name} modifier", blocktype);
                var playerKey = new StatKey($"player {blocktype.Name} modifier", blocktype);
                Stats.Add(ellaKey, 1);
                Stats.Add(playerKey, 1);
                _blockToKey.Add(blocktype, (ellaKey, playerKey));
            }

            foreach (StatKey key in Stats.Keys)
            {
                _percentModifiers.Add(key, 1);
                _incrementModifiers.Add(key, 0);
                StatListeners.Add(key, (f) => { });
            }

            BlockManager.Instance.OnBlockDestroy += AddCoins;
        }

        private void OnDestroy()
        {
            BlockManager.Instance.OnBlockDestroy -= AddCoins;
        }

        private void AddCoins(Block block)
        {
            AddCoins((int)(block.Value * GetStatValue(LUCK)));
        }

        public void AddCoins(int val){
            coins += val;
            CoinListener?.Invoke(coins);
            print("coin");
            Instantiate(_coinEffect, MinerManager.instance.transform.position + Vector3.up, Quaternion.identity).SetValue(val);
        }

        public void SubCoins(int val){
            coins -= val;
            CoinListener?.Invoke(coins);
        }

        /// <summary>
        /// Set the Multiplier of the given block type
        /// </summary>
        /// <param name="blockType">A block spec of the desired block type</param>
        /// <param name="value">The new value for the multiplier</param>
        public void setMultiplier(model.BlockSpec blockType, float value){
            if(BlockTypeMultipliers.ContainsKey(blockType)){
                BlockTypeMultipliers[blockType] = value;
            } else {
                BlockTypeMultipliers.Add(blockType, value);
            }
        }
    }
}
