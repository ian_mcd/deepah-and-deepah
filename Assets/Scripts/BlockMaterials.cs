using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deeper.model;
using System;

namespace deeper
{
    [CreateAssetMenu(menuName = "deeper/BlockMaterials", fileName = "BlockMaterials")]
    public class BlockMaterials : ScriptableObject
    {
        public AbstractBlockSpec[] Specs;
        public Material[] Materials;

        public Material GetMaterial(AbstractBlockSpec blockSpec)
        {
            int idx = Array.IndexOf(Specs, blockSpec);
            return Materials[idx];
        }
    }
}
