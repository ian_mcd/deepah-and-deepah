using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace deeper
{
    public class MinerManager : MonoBehaviour
    {
        // singleton
        private static MinerManager _Instance;
        public static MinerManager instance => _Instance ?  _Instance : _Instance = FindObjectOfType<MinerManager>();

        // stats
        public float autoDamage = 1;
        public Dictionary<model.BlockSpec, float> BlockTypeMultipliers = new Dictionary<model.BlockSpec, float>();
        public bool attacking = true;
        private Coroutine _currentRoutine;

        [SerializeField] private Animator _minerAnimator;
        // references
        private BlockManager blockManager => BlockManager.Instance;
        
        /// <summary>
        /// Get multiplier for the given block type
        /// </summary>
        /// <param name="blockType">A block spec of the desired block type</param>
        /// <returns>player multiplier</returns>
        public float getMultiplier(model.BlockSpec blockType){
            if(BlockTypeMultipliers.ContainsKey(blockType)){
                return BlockTypeMultipliers[blockType];
            }else{
                // default value for multiplier is 1
                BlockTypeMultipliers.Add(blockType, 1);
                return BlockTypeMultipliers[blockType];
            }
        }

        /// <summary>
        /// Set the Multiplier of the given block type
        /// </summary>
        /// <param name="blockType">A block spec of the desired block type</param>
        /// <param name="value">The new value for the multiplier</param>
        public void setMultiplier(model.BlockSpec blockType, float value){
            if(BlockTypeMultipliers.ContainsKey(blockType)){
                BlockTypeMultipliers[blockType] = value;
            }else{
                BlockTypeMultipliers.Add(blockType, value);
            }
        }

        private float secondsPerHit(){
            return 1 / PlayerManager.instance[PlayerManager.ELLA_SPEED];
        }
        
        private void Awake() {
            BlockManager.Instance.OnBlockReset += Fall;
        }

        public void Start()
        {
            StartCoroutine(attackLoop());
            PlayerManager.instance.StatListeners[PlayerManager.ELLA_SPEED] += UpdateAnimatorSpeed;
            UpdateAnimatorSpeed(PlayerManager.instance[PlayerManager.ELLA_SPEED]);
        }

        private void UpdateAnimatorSpeed(float speed)
        {
            _minerAnimator.SetFloat("Speed", speed);
        }

        public void Fall()
        {
            if (_currentRoutine != null) StopCoroutine(_currentRoutine);
            StartCoroutine(FallRoutine());
        }



        public IEnumerator FallRoutine()
        {
            float velocity = 0;
            float terminalVel = 10;
            float gravety = .8f;
            while (transform.position.y > BlockManager.Instance.Top.transform.position.y)
            {
                velocity += gravety;
                velocity = Mathf.Min(velocity, terminalVel);
                yield return null;
                transform.position -= new Vector3(0, velocity * Time.deltaTime, 0);
            }
            transform.position = new Vector3(transform.position.x, BlockManager.Instance.Top.transform.position.y, transform.position.z);
        }

            /// <summary>
            /// The miner will start attacking automatically based on its autoRate
            /// </summary>
            /// <returns>True if the cycle was started and false if the cycle
            /// was already active</returns>
            public bool startAttacking(){
            if(!attacking){
                attacking = true;
                StartCoroutine(attackLoop());
                return true;
            }
            return false;
        }

        /// <summary>
        /// Stop the miner from mining
        /// </summary>
        /// <returns>Return true if the miner is stopped and false if the miner
        /// was already stopped</returns>
        public bool stopAttacking(){
            if(attacking){
                attacking = false;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Performs the automining by the miner
        /// </summary>
        public IEnumerator attackLoop(){
            while(attacking){
                blockManager.Damage(PlayerManager.instance[PlayerManager.ELLA_DAMAGE]);
                BlockManager.Instance.Top.PlayDamageEffect();
                yield return new WaitForSeconds(secondsPerHit());
            }
            yield return null;
        }
    }
}
