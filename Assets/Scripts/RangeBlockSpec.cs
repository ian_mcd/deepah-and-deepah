using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace deeper.model
{
    [CreateAssetMenu(fileName = "RangeBlock", menuName = "deeper/RangeBlock")]
    public class RangeBlockSpec : AbstractBlockSpec
    {
        [SerializeField] private string _name;
        public override string Name => _name;

        [SerializeField] private float _minValue;
        [SerializeField] private float _maxValue;

        [SerializeField] private int _spawnRate;
        public override int SpawnRate => _spawnRate;
        public override float GenValue()
        {
            return Random.value * (_maxValue - _minValue) + _minValue;
        }

        [SerializeField] private float _hp;
        public override float GenHp() { return _hp; }


        public RangeBlockSpec(string name, int minValue, int maxValue, float hp)
        {
            _name = name;
            _minValue = minValue;
            _maxValue = maxValue;
            _hp = hp;
        }
    }
}