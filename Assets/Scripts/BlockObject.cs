using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deeper.model;
using UnityEngine.EventSystems;
using System;

namespace deeper
{
    public class BlockObject : MonoBehaviour, IPointerClickHandler, IDeletable
    {
        public Block Block;
        [SerializeField]private MeshRenderer _renderer;
        private Color _baseColor;
        public GameObject onDestroyParticleParent;
        public GameObject onDamageParticleParent;
        public event Action OnDamage;
        [SerializeField] private GameObject _clickEffect;


        private void Start()
        {
            _renderer.material.SetFloat("Vector1_33b7990513d949c9a2f61dd767f618c8", 1);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (this == BlockManager.Instance.Top)
            {
                BlockManager.Instance.Damage(PlayerManager.instance.GetStatValue(PlayerManager.PLAYER_DAMAGE));
                Instantiate(_clickEffect, eventData.pointerCurrentRaycast.worldPosition, Quaternion.FromToRotation(Vector3.forward, eventData.pointerCurrentRaycast.worldNormal));
            }
        }

        public void SetBlock(Block block)
        {
            this.Block = block;
            
        }

        public void SetColor(Block block)
        {
            // _renderer.material = BlockManager.Instance.Mats.GetMaterial((AbstractBlockSpec)block.BlockSpec);
            // _baseColor = _renderer.material.color;
        }

        public void PlayDamageEffect()
        {
            if (onDamageParticleParent != null)
            {
                foreach (Transform psObj in onDamageParticleParent.transform)
                {
                    psObj.GetComponent<ParticleSystem>().Play();
                }
            }
        }

        public void PlayDestroyEffect()
        {
            if (onDestroyParticleParent != null)
            {
                foreach (Transform psObj in onDamageParticleParent.transform)
                {
                    psObj.GetComponent<ParticleSystem>().Play();
                }
            }
        }

        public void Damage(float damage)
        {
            _renderer.material.SetFloat("Vector1_33b7990513d949c9a2f61dd767f618c8", (Block.Hp / Block.StartHp));
            Block.Damage(damage);
            OnDamage?.Invoke();
        }

        public void Delete()
        {
            Destroy(gameObject);
        }
    }
}
