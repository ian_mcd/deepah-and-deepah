using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using deeper.model;
using System;
using Random = System.Random;

namespace deeper
{
    public class BlockManager : MonoBehaviour
    {
        private static BlockManager _instance;
        public static BlockManager Instance => (_instance) ? _instance : _instance = FindObjectOfType<BlockManager>();

        public List<AbstractBlockSpec> BlockTypes;
        [SerializeField] BlockObject _blockPrefab;

        private Queue<BlockObject> _blockQueue = new Queue<BlockObject>();
        [SerializeField] private int _stackCount = 5;

        public event Action<BlockObject, int> OnBlockSpawn;
        public event Action<Block> OnBlockDestroy;
        public event Action<Block, float> OnBlockDamage;
        public event Action OnBlockReset;

        private int _blockNumber = 0;
        private float _blockHeight = 1;
        public float BlockLevel => -_blockNumber * _blockHeight;

        public BlockObject Top => _blockQueue.Peek();
        public BlockMaterials Mats;

        private Random _random;

        private Dictionary<Block, BlockObject> _blockObjectDict = new Dictionary<Block, BlockObject>();

        private int _cacheOnFrameCount = -1;
        private int _totalBlockSpawnRange;

        private DestructionManager _dm = new DestructionManager();
        [SerializeField] private BlockPrefabs _blockPrefabs;
        private void Awake()
        {
            _random = new Random();
            ResetStack();
        }

        private Block GenerateBlock()
        {
            int currFrame = Time.frameCount;
            if (currFrame != _cacheOnFrameCount)
            {
                _totalBlockSpawnRange = 0;
                foreach (AbstractBlockSpec bs in BlockTypes)
                    _totalBlockSpawnRange += bs.SpawnRate;
            }

            int blockSpawnNum = _random.Next(_totalBlockSpawnRange);
            AbstractBlockSpec blockSpec = BlockTypes[BlockTypes.Count - 1];
            foreach (AbstractBlockSpec bs in BlockTypes)
            {
                blockSpawnNum -= bs.SpawnRate;
                if (blockSpawnNum <= 0)
                {
                    blockSpec = bs;
                    break;
                }
            }

            return new Block(blockSpec);
        }

        public void Damage(float damage)
        {
            while (damage >= float.Epsilon && _blockQueue.Count != 0)
            {
                BlockObject bo = _blockQueue.Peek();
                Block block = bo.Block;

                float blockDamage = Mathf.Min(block.Hp, damage);
                damage -= block.Hp;
                bo.Damage(blockDamage);
                OnBlockDamage?.Invoke(block, blockDamage);
                if (block.Hp < float.Epsilon)
                {
                    _blockQueue.Dequeue();
                    _blockObjectDict.Remove(block);
                    
                    OnBlockDestroy?.Invoke(bo.Block);
                    _dm.Add(bo);
                }
            }
            while (damage >= float.Epsilon)
            {
                Block block = GenerateBlock();
                _blockNumber++;
                float blockDamage = Mathf.Min(block.Hp, damage);

                damage -= block.Hp;

                block.Damage(blockDamage);
                OnBlockDamage?.Invoke(block, blockDamage);

                if (block.Hp < float.Epsilon)
                    OnBlockDestroy?.Invoke(block);
            }
            _dm.Destroy(.2f * PlayerManager.instance[PlayerManager.ELLA_SPEED]);
            ResetStack();
            OnBlockReset?.Invoke();
        }

        public BlockObject GetBlockObject(Block block)
        {
            BlockObject bo;
            if (_blockObjectDict.TryGetValue(block, out bo))
                return bo;
            return null;
        }
 
        private void ResetStack()
        {
            while (_blockQueue.Count < _stackCount)
            {
                Block b = GenerateBlock();
                AbstractBlockSpec bs = (AbstractBlockSpec) b.BlockSpec;
                BlockObject bo;
                if (_blockPrefabs.Contains(bs))
                {
                    bo = Instantiate(_blockPrefabs.GetPrefab(bs), new Vector3(0, BlockLevel, 0), Quaternion.identity);
                }
                else
                {
                    bo = Instantiate(_blockPrefab, new Vector3(0, BlockLevel, 0), Quaternion.identity);
                    bo.SetColor(b);
                }
                bo.SetBlock(b);
                _blockQueue.Enqueue(bo);
                OnBlockSpawn?.Invoke(bo, _blockQueue.Count - 1);
                _blockNumber++;
                _blockObjectDict.Add(b, bo);

            }
        }
    }
}
