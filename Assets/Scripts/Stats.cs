using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace deeper.model
{
    public struct StatKey
    {
        public string Name;
        public object Target;

        public StatKey(string name, object target=null)
        {
            Name = name;
            Target = target;
        }

        public override int GetHashCode()
        {
            return new { Name, Target }.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }
    }
}