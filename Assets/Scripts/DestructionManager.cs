using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace deeper.model
{
    public interface IDeletable
    {
        void Delete();
    }

    public class DestructionManager
    {
        private Queue<IDeletable> _destructionQueue = new Queue<IDeletable>();

        public void Add(IDeletable obj)
        {
            _destructionQueue.Enqueue(obj);
        }

        public void Destroy(float destroyTime=.2f)
        {
            PlayerManager.instance.StartCoroutine(DestroyRoutine(destroyTime));
        }

        private IEnumerator DestroyRoutine(float destroyTime)
        {
            var wait = new WaitForSeconds(.2f);
            while(_destructionQueue.Count != 0)
            {
                IDeletable obj = _destructionQueue.Dequeue();
                obj.Delete();
                yield return wait;
            }
        }
    }
}
